<?php
require_once 'vendor/autoload.php';

function loadFile(string $url): string
{
    $cacheDir = __DIR__ . '/cache/html/';
    if (!is_dir($cacheDir))
    {
        mkdir($cacheDir, 0777, true);
    }

    $fileName = substr($url, strripos($url, '/'));
    $localFile = $cacheDir . $fileName;

    if (!is_file($localFile))
    {
        $files = scandir($cacheDir);
        if (count($files) > 1)
        {
            foreach ($files as $fileName)
            {
                $file = $cacheDir . $fileName;
                if (is_file($file))
                {
                    unlink($file);
                }
            }
        }

        $file = fopen($localFile, 'w+');

        $ch = curl_init();
        curl_setopt_array($ch, array (
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_ACCEPT_ENCODING => 'gzip',
                CURLOPT_FILE => $file,
                CURLOPT_URL => $url
        ));

        $result = curl_exec($ch);
        $error = curl_error($ch);

        curl_close($ch);
        fclose($file);

        if (!$result)
        {
            throw new Exception('Error downloading file: ' . $error);
        }
    }

    return file_get_contents($localFile);
}

function getVPläne(): array
{
    $json = (new DSBMobileConnector("211382", "hegvplan"))->getData();

    $vpläne = array ();
    foreach ($json['ResultMenuItems'] as $menuItem)
    {
        if ($menuItem['Title'] !== 'Inhalte')
            continue;
        foreach ($menuItem['Childs'] as $inhalt)
        {
            if ($inhalt['Title'] !== 'Pläne')
                continue;
            foreach ($inhalt['Root']['Childs'] as $plan)
            {
                if ($plan['Title'] === 'Pausenhalle heute')
                {
                    $vpläne['today'] = loadFile($plan['Childs'][0]['Detail']);
                }
                else if ($plan['Title'] === 'Pausenhalle morgen')
                {
                    $vpläne['tomorrow'] = loadFile($plan['Childs'][0]['Detail']);
                }
            }
        }
    }

    return $vpläne;
}

function getFakeVPläne(): array
{
    return array (
            'today' => file_get_contents(__DIR__ . '/testData/thursday.html'),
            'tomorrow' => file_get_contents(__DIR__ . '/testData/friday.html')
    );
}

$vpläne = getVPläne();
//$vpläne = getFakeVPläne();

$source = $vpläne[$_GET['day'] ?? null] ?? 'list';

if ($source === 'list')
{
    foreach ($vpläne as $key => $value)
    {
        $parser = new DSBMobileVPlanParser($value);
        echo '<a href="?day=' . $key . '">' . $parser->getDateString() . '</a><br>';
    }
    exit(0);
}

header('Content-Type: application/json;charset=utf-8');
echo json_encode((new DSBMobileVPlanParser($source))->parse());
