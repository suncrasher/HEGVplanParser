<?php

/**
 * A relatively simple class to retrieve the (json) data from DSBMobile
 *
 * @author suncrasher
 */
class DSBMobileConnector
{
    /** @var string */
    private static $loginURL = 'https://www.dsbmobile.de/Login.aspx';
    /** @var string */
    private static $dataURL = 'https://www.dsbmobile.de/JsonHandlerWeb.ashx/GetData';
    /** @var string */
    private $cookiefile;
    /** @var resource */
    private $ch;
    /** @var string */
    private $user;
    /** @var string */
    private $password;
    /** @var bool */
    private $cacheCookies;

    /**
     * Creates a DSBMobileConnector with the specified user and password
     *
     * @param string $user The Username
     * @param string $password The Password
     * @param bool $cacheCookies If the cookies should be cached between requests and a login should only be attempted if the old session expired
     */
    public function __construct(string $user, string $password, bool $cacheCookies = true)
    {
        $this->user = $user;
        $this->password = $password;
        $this->cacheCookies = $cacheCookies;

        $tmpDir = __DIR__ . '/../cache/cookie/';
        if (!is_dir($tmpDir))
        {
            mkdir($tmpDir, 0777, true);
        }

        $this->cookiefile = $tmpDir . 'cookies' . $user . '#' . $password;

        if ($cacheCookies)
        {
            $this->initCURL();
        }
        else
        {
            $this->login();
        }
    }

    /**
     * Closes the internal cURL handle and deletes the cookie file if it shouldn't be cached
     */
    public function __destruct()
    {
        curl_close($this->ch);
        if (!$this->cacheCookies)
        {
            unlink($this->cookiefile);
        }
    }

    /**
     * Closes the curretn cURL handle, deletes the cookie file if specified and then creates a new cURL handle
     *
     * @param bool $deleteCookieFile If the cookie file should be deleted to remove all curretn cookies
     */
    private function initCURL(bool $deleteCookieFile = false): void
    {
        if ($this->ch)
        {
            curl_close($this->ch);
        }

        if ($deleteCookieFile)
        {
            unlink($this->cookiefile);
        }

        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_COOKIEJAR, $this->cookiefile);
        curl_setopt($this->ch, CURLOPT_COOKIEFILE, $this->cookiefile);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
    }

    /**
     * Performs a Login with the stored User and Password
     *
     * @throws Exception if the cURL Request fails
     */
    private function login(): void
    {
        $this->initCURL(true);

        curl_setopt($this->ch, CURLOPT_URL, self::$loginURL);

        $result = curl_exec($this->ch);
        if (!$result)
        {
            throw new Exception('Couldn\'t Login! Error: ' . curl_error($this->ch));
        }

        $postfields = http_build_query(array (
                'txtUser' => $this->user,
                'txtPass' => $this->password,
                '__VIEWSTATE' => self::findValue('__VIEWSTATE', $result),
                '__EVENTVALIDATION' => self::findValue('__EVENTVALIDATION', $result),
                'ctl03' => 'Anmelden'
        ));

        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postfields);

        $result = curl_exec($this->ch);

        if (!$result)
        {
            throw new Exception('Couldn\'t Login! Error: ' . curl_error($this->ch));
        }
    }

    /**
     * Retrieves the data from DSBMobile and returns it in form of an array
     *
     * @param bool $allowLogin if a login should be attempeted in case the data retrieval fails
     * @throws Exception if the cURL Request or data retrieval fails
     * @return array The retirieved data in json form
     */
    public function getData(bool $allowLogin = true): array
    {
        curl_setopt($this->ch, CURLOPT_URL, self::$dataURL);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, '{"req":{"Data":"H4sIAAAAAAAAC6tWcirNS8lJ9UxRslJSqgUA9xQVbg8AAAA=","DataType":1}}');
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array (
                'Content-Type: application/json'
        ));
        $result = curl_exec($this->ch);

        if (!$result)
        {
            throw new Exception('Couldn\'t Retrieve Data! Error: ' . curl_error($this->ch));
        }

        $data = json_decode($result, true)['d'];
        if ($data === '')
        {
            if ($allowLogin)
            {
                $this->login();
                return $this->getData(false);
            }
            else
            {
                throw new Exception('Could not retrieve data! Login invalid?');
            }
        }

        return json_decode(self::decode($data), true);
    }

    /**
     * Finds the value of an HTML element with the specified id in the document,
     * given the id atribute is directly in front of the value attribute with exactly on space between them and both values are surrounded by double quotes (")<br>
     * For Example:<br>
     * id="theId" value="theValue"
     *
     * @param string $value The id of the value to search for
     * @param string $document The HTML document in which to search for the value
     * @return string The value if found, otherwise an empty string
     */
    private static function findValue(string $value, string $document): string
    {
        $matches = array ();
        preg_match('/.*?id="' . preg_quote($value) . '" value="(.*?)".*/', $document, $matches);
        return $matches[1] ?? '';
    }

    /**
     * Encodes the string in the same way DSBMobile does for use with it
     *
     * @param string $string The string to encode
     * @return string The encoded string
     */
    private static function encode(string $string): string
    {
        return base64_encode(gzencode($string));
    }

    /**
     * Decodes the string encoded by DSBMobile
     *
     * @param string $data The encoded string
     * @return string The decoded string
     */
    private static function decode(string $data): string
    {
        return gzdecode(base64_decode($data));
    }
}
