<?php

/**
 * A class for parsing a vplan from DSBMobile and retrieving information about it
 *
 * @author sven
 */
class DSBMobileVPlanParser
{
    /** @var array */
    private $changes;
    /** @var string */
    private $date;
    /** @var DOMXPath */
    private $xpath;

    /** @var string */
    private $nbspChar;

    /**
     * Creates a DSBMobileVPlanParser with the specified vplan HTML document as string as source
     *
     * @param string $data The HTML data
     */
    public function __construct(string $data)
    {
        $this->nbspChar = chr(0xC2) . chr(0xA0);

        $doc = new DOMDocument();
        $doc->preserveWhiteSpace = false;
        $doc->loadHTML($data);

        $this->xpath = new DOMXPath($doc);
    }

    /**
     * Fully parses the vplan and returns the changes as an associative array
     * The format of the array:
     *     classID+:
     *         lessonNumber+:
     *             lesson: The lesson as entered in the original vplan, or "---" for canceled lessons
     *             teacher: The two letter teacher abbreviation, or null if canceled
     *             room: The room, or null if canceled
     *             note: The note entered
     *             length: The length of the change
     *
     * @param $classes An array of the class ids for which to parse the data
     * @return array The vplan as an array
     */
    public function parse(array $classes = null): array
    {
        $classQuery = '//tr[td[contains(@class, "inline_header")]]';

        $rawChanges = array ();
        foreach ($this->xpath->query($classQuery) as $class)
        {
            $className = $class->childNodes[0]->nodeValue;
            $classId = substr($className, 0, strpos($className, " "));
            if ($classes === null || in_array($classId, $classes))
            {
                $classChanges = array ();

                $nextChange = $class;
                while ($nextChange = $nextChange->nextSibling)
                {
                    $cells = $nextChange->childNodes;
                    if ($cells->length !== 5)
                    {
                        break;
                    }
                    $time = $cells[0]->nodeValue;
                    $teacher = $cells[1]->nodeValue;
                    $lesson = $cells[2]->nodeValue;
                    $room = $cells[3]->nodeValue;
                    $note = $cells[4]->nodeValue;

                    $classChanges[$time] = array (
                            'lesson' => trim($lesson, $this->nbspChar),
                            'teacher' => trim($teacher, $this->nbspChar),
                            'room' => trim($room, $this->nbspChar),
                            'note' => trim($note, $this->nbspChar),
                            'length' => 1
                    );
                }
                $rawChanges[$classId] = $classChanges;
            }
        }

        $finalChanges = array ();
        foreach ($rawChanges as $classId => $changes)
        {
            $finalChanges[$classId] = array ();
            foreach ($changes as $time => $change)
            {
                $newChanges = array ();
                $newChanges[$time] = $change;
                if ($change['lesson'] === '---')
                {
                    $newChanges[$time] = array (
                            'lesson' => '---',
                            'teacher' => null,
                            'room' => null,
                            'note' => $change['note'],
                            'length' => 1
                    );
                }

                if (preg_match('/^(\d+) ?- ?(\d+)$/', $time, $match))
                {
                    $start = $match[1];
                    $end = $match[2];
                    $newChanges[$start] = $newChanges[$time];
                    $newChanges[$start]['length'] = $end - $start + 1;
                    unset($newChanges[$time]);
                    $time = $start;
                }

                $search = $time;
                do
                {
                    $search--;
                }
                while (!isset($finalChanges[$classId][$search]) && $search > 0);
                if ($search > 0)
                {
                    $newChange = $newChanges[$time];
                    $searchChange = $finalChanges[$classId][$search];
                    if ($searchChange['lesson'] === $newChange['lesson'] && $searchChange['teacher'] === $newChange['teacher'] && $searchChange['room'] === $newChange['room'])
                    {
                        $finalChanges[$classId][$search]['length'] += $newChange['length'];
                        unset($newChanges[$time]);
                    }
                }
                $finalChanges[$classId] += $newChanges;
            }
        }
        return $finalChanges;
    }

    /**
     * Returns the date as printed in the document
     *
     * @return string The date
     */
    public function getDateString(): string
    {
        if (!$this->date)
        {
            $query = '//div[@class="mon_title"]';

            $this->date = trim($this->xpath->query($query)[0]->nodeValue, $this->nbspChar);
        }
        return $this->date;
    }

    /**
     * Returns a DateTime Object for the day the vplan is for
     *
     * @return DateTime The DateTime Object
     */
    public function getDateObject(): DateTime
    {
        $date = $this->getDateString();
        return new DateTime(substr($date, 0, strripos($date, ' ')));
    }

    /**
     * Returns the german word for the week say the vplan is for
     *
     * @return string The day
     */
    public function getDateWord(): string
    {
        $date = $this->getDateString();
        return substr($date, strripos($date, ' '));
    }
}
